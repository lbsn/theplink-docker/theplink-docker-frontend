![version](https://lbsn.vgiscience.org/theplink-docker/theplink-docker-frontend/version.svg) ![pipeline](https://lbsn.vgiscience.org/theplink-docker/theplink-docker-frontend/pipeline.svg)

# theplink-docker.frontend

The frontend for theplink-docker is maintained in this separate repository and imported as a git submodule. This makes independent development possible, it can be tested and developed with proper base path (`/frontend`) and follows the separation of concerns principle. 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```
