const env = process.env.VUE_APP_ENV;

let envApiUrl = '';

if (env === 'production') {
  envApiUrl = `https://${process.env.VUE_APP_DOMAIN_PROD}`;
} else if (env === 'staging') {
  envApiUrl = `https://${process.env.VUE_APP_DOMAIN_STAG}`;
} else {
  envApiUrl = `http://${process.env.VUE_APP_DOMAIN_DEV}`;
}


const mapBoundsLngLeft: number = parseFloat(process.env.VUE_APP_MAP_BOUNDS_LNG_LEFT);
const mapBoundsLngRight: number = parseFloat(process.env.VUE_APP_MAP_BOUNDS_LNG_RIGHT);
const mapBoundsLatTop: number = parseFloat(process.env.VUE_APP_MAP_BOUNDS_LAT_TOP);
const mapBoundsLatBottom: number = parseFloat(process.env.VUE_APP_MAP_BOUNDS_LAT_BOTTOM);
const mapCenterLng: number = mapBoundsLngLeft + (mapBoundsLngRight - mapBoundsLngLeft) / 2;
const mapCenterLat: number = mapBoundsLatBottom + (mapBoundsLatTop - mapBoundsLatBottom) / 2;

export const apiUrl = envApiUrl;
export const appName = process.env.VUE_APP_NAME;
export const mapBounds: number[] = [
  mapBoundsLngLeft, mapBoundsLatBottom, mapBoundsLngRight, mapBoundsLatTop];
export const mapCenter: number[] = [mapCenterLng, mapCenterLat];
